package com.tigres.site;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UkmMemberRegistrationBackendApplication {

	protected static final Logger logger = LoggerFactory.getLogger(UkmMemberRegistrationBackendApplication.class);
	
	public static void main(String[] args) {
		logger.info("============================= Running backend ukm member registration =============================");
		SpringApplication.run(UkmMemberRegistrationBackendApplication.class, args);
	}

}
