package com.tigres.site.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.tigres.model.TestingModel;

@Mapper
public interface TestingDao {

	 public TestingModel getByNim(@Param("nim") String nim);
	
}
