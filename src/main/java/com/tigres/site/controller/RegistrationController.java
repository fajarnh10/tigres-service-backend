package com.tigres.site.controller;

import java.io.IOException;
import java.util.Date;
import java.util.Scanner;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.tigres.constant.ConstantCodes;
import com.tigres.constant.ConstantSymbol;
import com.tigres.model.TestingModel;
import com.tigres.model.test.TestingModelRequest;
import com.tigres.model.test.TestingModelResponse;
import com.tigres.site.dao.TestingDao;

@RestController
public class RegistrationController {

	protected static final Logger logger = LoggerFactory.getLogger(RegistrationController.class);
	
	@Autowired
	private ObjectMapper mapper = new ObjectMapper();
	
	@Autowired
	private TestingDao testingDao;
	
	private TestingModelResponse responseInquiry = new TestingModelResponse();
	
	private TestingModelRequest requestInquiry = new TestingModelRequest(); 
	
	@RequestMapping(path="test/v1/inquiry/student", method = RequestMethod.POST)
	public void getDataRegistration(HttpServletRequest request, HttpServletResponse response) {
		try {
			logger.info("==========START INCOMMING INQUIRY REQUEST START==========");
			
			responseInquiry.setStatus(ConstantCodes.CONST_STRING_CODE_GENERAL_ERROR);
			
			StringBuffer sb = new StringBuffer();
		    @SuppressWarnings("resource")
			Scanner scanner = new Scanner(request.getInputStream());
		    while (scanner.hasNextLine()) {
		        sb.append(scanner.nextLine());
		    }
		    String body = sb.toString();
			logger.info("Request Body : " + body);
			if (body != null && !ConstantSymbol.CONST_SYMBOL_EMPTY.equals(body)) {
				requestInquiry = mapper.readValue(body, TestingModelRequest.class);
				logger.info("Mapping Request To Model : " + mapper.writeValueAsString(requestInquiry));
				if (requestInquiry != null) {
					if (requestInquiry.getNim() != null && !requestInquiry.getNim().isEmpty()) {
						try {
							TestingModel responseData = testingDao.getByNim(requestInquiry.getNim());
							if (responseData != null) {
								responseInquiry.setNim(responseData.getNim());
								responseInquiry.setFrontName(responseData.getFrontName());
								responseInquiry.setBackName(responseData.getBackName());
								responseInquiry.setProdi(responseData.getProdi());
								responseInquiry.setFaculty(responseData.getFaculty());
								responseInquiry.setStatus("000");
								
								logger.info("INQUIRY SUCCESS, WITH RESPONSE : ");
								logger.info(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(responseInquiry));
							}else {
								responseInquiry.setNim(responseData.getNim());
								responseInquiry.setFrontName(responseData.getFrontName());
								responseInquiry.setBackName(responseData.getBackName());
								responseInquiry.setProdi(responseData.getProdi());
								responseInquiry.setFaculty(responseData.getFaculty());
								logger.info("INQUIRY SUCCESS, DATA IS NOT EXIST");
								responseInquiry.setStatus("990");
							}
							logger.info("");
						} catch (Exception e) {
							logger.error("ERROR SQL GET DATA FROM DB : \n",e);
							responseInquiry.setStatus("211");
						}
						
					} else {
						logger.error("ERROR PARAMETER NIM REQUEST BODY CANNOT BE NULL");
						responseInquiry.setStatus(ConstantCodes.CONST_STRING_CODE_INQUIRY_STUDENT_NIM_NULL);
					}
				} else {
					logger.error("ERROR MAPPING REQUEST BODY TO MODEL");
					responseInquiry.setStatus(ConstantCodes.CONST_STRING_CODE_REQUEST_BODY_NULL);
				}
			} else {
				logger.error("ERROR REQUEST BODY NULL");
				responseInquiry.setStatus(ConstantCodes.CONST_STRING_CODE_REQUEST_BODY_NULL);
			}
		} catch (Exception e) {
			logger.error("General Error When Inquiry Student : \n",e);
		}
		
		try {
			mapper.enable(SerializationFeature.INDENT_OUTPUT);
		    String resp = mapper.writeValueAsString(responseInquiry);
		    logger.info("return response "+resp);
			responseJSON(response, resp);
		} catch (Exception e) {
			logger.error("ERROR WHEN SEND RESPONSE : \n",e);
		}
		
		logger.info("==========END INQUIRY REQUEST END==========");
	}
	
	private void responseJSON(HttpServletResponse response, String json) {
		response.setStatus(HttpServletResponse.SC_OK); // status
		response.setDateHeader("Date", new Date().getTime()); // Date
		response.setContentType("application/json"); // content
		response.setContentLength(json.length()); // content-length
		try {
			response.getWriter().print(json);
			response.getWriter().flush();
			response.getWriter().close();
		} catch (IOException e) {
			logger.error("Error while generate json", e);
		}
	}
	
	public static void main(String[] args) {
		System.out.print(ConstantSymbol.CONST_SYMBOL_EMPTY);
	}
	
}
